cask 'systhist' do
  version '1.7'
  sha256 'fa01dc3654e7eabb6e654dfd7b55b07492068fca684c4d7fd97f6ef0025b4940'

  # eclecticlightdotcom.files.wordpress.com was verified as official when first introduced to the cask
  url 'https://eclecticlightdotcom.files.wordpress.com/2018/11/systhist17.zip'
  name 'systhist'
  homepage 'https://eclecticlight.co/2018/11/17/lockrattler-4-16-and-systhist-1-7-are-now-available/'

  app 'systhist17/SystHist.app'
end
